import tensorrt as trt
from layers import *

def build_engine(enc_weights, dec_weights, logger,
                 enc_states, dec_states, enc_chs, dec_chs,
                 batch, img_height, img_width):
    builder = trt.Builder(logger)

    profile_0 = builder.create_optimization_profile()
    profile_0.set_shape("input", (batch,3,img_height,img_width), (batch,3,img_height,img_width), (batch,3,img_height,img_width))

    network = builder.create_network(flags=1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH))
    config = builder.create_builder_config()
    config.max_workspace_size = 1<<30
    config.add_optimization_profile(profile_0)

    # Populate the network using weights from the PyTorch model.
    populate_network(network, enc_weights, dec_weights, 
                     enc_states, dec_states, enc_chs, dec_chs,
                      batch, img_height, img_width)
    
    # Build and return an engine.
    plan = builder.build_serialized_network(network, config)
    with open("depth.engine", "wb") as f:
        f.write(plan)

def load_engine(runtime, engine_path="depth.engine"):
    with open(engine_path, 'rb') as f:
        engine_data = f.read()
    engine = runtime.deserialize_cuda_engine(engine_data)
    return engine

def populate_network(network, enc_weights, dec_weights, enc_states, dec_states, enc_chs, dec_chs,
                     batch, img_height, img_width):
    input_tensor = network.add_input(name="input", dtype=trt.float32, shape=(6,3,192,640))
    enc_layer_outputs = []
    dec_layer_outputs = []
    downsample_outputs = []
    features = []

    for i in range(len(enc_states)):
        if i == 0:
            conv_weights = get_weights(enc_weights, enc_states[i], "weight", sub_layer=None)
            conv1 = network.add_convolution(input_tensor, 
                                            enc_chs[i], 
                                            kernel_shape=(7,7), 
                                            kernel = conv_weights)
            conv1.stride = (2,2)
            conv1.padding = (3,3)
            enc_layer_outputs.append(conv1.get_output(0))
            continue

        if i == 1:
            bn1 = batchnorm(network, enc_layer_outputs[-1], enc_weights, enc_states[i], sub_layer=None)

            relu1 = relu(network, bn1.get_output(0))
            features.append(relu1.get_output(0))

            maxpool1 = maxpool(network, relu1.get_output(0))
            enc_layer_outputs.append(maxpool1.get_output(0))
            continue
        
        if i in [2, 3, 5, 7, 9]:
            stride = (1, 1)
        else:
            stride = (2, 2)

        identity = enc_layer_outputs[-1]

        layer_conv1 = conv3x3(network, enc_weights, enc_layer_outputs[-1], enc_chs[i], enc_states[i], "conv1", stride=stride, padding=(1,1))

        layer_bn1 = batchnorm(network, layer_conv1.get_output(0), enc_weights, enc_states[i], "bn1")

        layer_relu1 = relu(network, layer_bn1.get_output(0))
        
        layer_conv2 = conv3x3(network, enc_weights, layer_relu1.get_output(0), enc_chs[i], enc_states[i], "conv2", stride=(1,1), padding=(1,1))

        layer_bn2 = batchnorm(network, layer_conv2.get_output(0), enc_weights, enc_states[i], "bn2")
        enc_layer_outputs.append(layer_bn2.get_output(0))

        if i in range(4, 10, 2):
            downsample_conv = conv1x1(network, enc_weights, identity, enc_chs[i], enc_states[i], "downsample.0", stride=(2,2))

            downsample_bn = batchnorm(network, downsample_conv.get_output(0), enc_weights, enc_states[i], "downsample.1")
            identity = downsample_bn.get_output(0)

        identity_layer = network.add_elementwise(identity, enc_layer_outputs[-1], op=trt.ElementWiseOperation.SUM)

        layer_relu2 = relu(network, identity_layer.get_output(0))
        enc_layer_outputs.append(layer_relu2.get_output(0))

        if i in [3, 5, 7, 9]:
            features.insert(0, enc_layer_outputs[-1])
    
    for idx, state in enumerate(dec_states):
        if idx == 0:
            input = features[idx]
        else:
            input = dec_layer_outputs[-1]

        upconv_a = conv3x3(network, dec_weights, input, dec_chs[idx], dec_states[state][0], "conv", padding=(1,1))
        upconv_a.bias = get_weights(dec_weights, dec_states[state][0], "bias", "conv")

        elu_a = elu(network, upconv_a.get_output(0))

        upsample_a = upsample(network, elu_a.get_output(0))

        upsample_output = upsample_a.get_output(0)
        dec_layer_outputs.append(upsample_output)

        if idx < 4:
            concatenate = network.add_concatenation([upsample_output, features[idx+1]])
            dec_layer_outputs.append(concatenate.get_output(0))

        upconv_b = conv3x3(network, dec_weights, dec_layer_outputs[-1], dec_chs[idx], dec_states[state][1], "conv", padding=(1,1))
        upconv_b.bias = get_weights(dec_weights, dec_states[state][1], "bias", "conv")

        elu_b = elu(network, upconv_b.get_output(0))

        dec_layer_outputs.append(elu_b.get_output(0))

        if idx == 4:
            dispconv = conv3x3(network, dec_weights, upconv_b.get_output(0), 1, dec_states[state][2], sub_layer=None, padding=(1,1))

            dispconv.bias = get_weights(dec_weights, dec_states[state][2], "bias", sub_layer=None)

            sigmoid = network.add_activation(dispconv.get_output(0), type=trt.ActivationType.SIGMOID)
            output = sigmoid.get_output(0)           

    network.mark_output(tensor=output)