import os
import cv2
import json
import rospy
import torch
import numpy as np
import tensorrt as trt
import pycuda.driver as cuda

from cv_bridge import CvBridge
from std_msgs.msg import String
from sensor_msgs.msg import Image, DisparityImage
from ml_msgs.msg import ImagesArray

from utils import load_engine, build_engine

TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

class HostDeviceMem(object):
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()

class DepthEstimation(HostDeviceMem):
    def __init__(self):
        # Locate configuration files
        CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))

        with open(CURRENT_DIR+'/../config/depth_configs.json') as fp:
            config = json.load(fp)

        # Set parameters
        # For image input
        self.batch_size = config["batch_size"]
        self.full_res_H = config["full_res_height"]
        self.full_res_W= config["full_res_width"]
        self.scale = config["scale"]
        self.inf_H = self.full_res_H // self.scale
        self.inf_W = self.full_res_W // self.scale

        # For depth output
        self.depth_scale = config["depth_scale"]

        # For models
        self.enc_states = config["enc_states"]
        self.dec_states = config["dec_states"]
        self.enc_chs = config["enc_chs"]
        self.dec_chs = config["dec_chs"]

        # Load trained model weights
        enc_model_path = CURRENT_DIR + '/..' + config['enc_model_path']
        enc_weights = torch.load(enc_model_path, map_location=torch.device("cpu"))
        dec_model_path = CURRENT_DIR + '/..' + config['dec_model_path']
        dec_weights = torch.load(dec_model_path, map_location=torch.device("cpu"))
        
        # Load trt engine if available, if not, build an engine
        if not os.path.isfile("depth.engine"):
            self.build_engine(enc_weights, dec_weights, TRT_LOGGER, 
                              self.enc_states, self.dec_states,
                              self.enc_chs, self.dec_chs,
                              self.batch_size, self.inf_H, self.inf_W)
        self.runtime = trt.Runtime(TRT_LOGGER)
        self.engine = self.load_engine(self.runtime)
        self.context = self.engine.create_execution_context()

        # Define pub-subs
        rospy.Subscriber('/decoded', ImagesArray, self.image_callback, queue_size=1)
        self.pub = rospy.Publisher('/ml/disparity', DisparityImage, queue_size=1)
    
    def image_callback(self, msg):
        # Get camera data from subscribed topic -> /decoded
        data = np.fromstring(msg.data, np.uint8)
        channel = msg.step//msg.width
        data = np.reshape(data, (msg.length, msg.height, msg.width, channel))
        images_list = np.split(data, 9)

        # Replicating pytorch transform ToTensor + Normalize
        for image in images_list:
            image = self.transformation_gpu(image)
        images = np.concatenate(images_list, dim=0)

        # Run image through model for inference
        output = self.process_batch(images, self.ngine, self.context)

        # Publish disparity image
        self.pub.publish(output)

    def process_batch(self, images, engine, context):
        inputs, outputs, bindings, stream = self.allocate_buffers(engine)
        inputs[0].host = images
        [output] = self.do_inference(context, bindings=bindings,
                                     inputs=inputs, outputs=outputs, stream=stream)
        return self.post_process(output)

    def transformation_gpu(self, image_array):
        """
        Input: image_array, an array of images from cv2 loading or any numpy array.
        Return: Transformed image equivalent to torchvision transform of ToTensor()+Normalise()

        Torchvision standard method:
        Most accurate match to pytorch torvision library (but transpose use CPU):
        #image = torch.from_numpy(image_array.transpose((2,0,1))).contiguous()
        #image = image.to(dtype=torch.float32).div(255).cuda() 
        """
        # Transfer to GPU
        image = torch.from_numpy(image_array).contiguous().cuda()
        image = image.permute((0,3,1,2)).to(dtype=torch.float32).div(255) # rearrange image array from BHWC to BCHW
        image = image[:,[2,1,0],:,:] #note image array is in the form of BGR, u need RGB (reduce 20% cpu load from cv2.cvtColor)
        image = (image - 0.45) / 0.225
        return image
    
    def post_process(self, output):
        reshaped_output = np.reshape(output, (self.batch_size, 1, self.inf_H, self.inf_W), order='C')
        outputs = np.split(reshaped_output, self.batch_size, 0)
        for i in range(len(outputs)):
            resized_output = torch.nn.functional.interpolate(
                torch.from_numpy(outputs[i]), (self.full_res_H, self.full_res_W),
                mode='bilinear', align_corners=False
            )
            disp_resized_np = resized_output.squeeze().cpu().numpy()
            outputs[i] = disp_resized_np * self.depth_scale
        return outputs
    
    def allocate_buffers(engine):
        """
        Allocate memory buffers required for the engine
        """
        inputs = []
        outputs = []
        bindings = []
        stream = cuda.Stream()
        for binding in engine:
            print(binding)
            print(engine.get_binding_shape(binding))
            size = trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size
            dtype = trt.nptype(engine.get_binding_dtype(binding))
            # Allocate host and device buffers
            host_mem = cuda.pagelocked_empty(size, dtype)
            device_mem = cuda.mem_alloc(host_mem.nbytes)
            # Append the device buffer to device bindings.
            bindings.append(int(device_mem))
            # Append to the appropriate list.
            if engine.binding_is_input(binding):
                inputs.append(HostDeviceMem(host_mem, device_mem))
            else:
                outputs.append(HostDeviceMem(host_mem, device_mem))
        return inputs, outputs, bindings, stream
    
    def do_inference(context, bindings, inputs, outputs, stream):
        # Transfer input data to the GPU.
        [cuda.memcpy_htod_async(inp.device, inp.host, stream) for inp in inputs]

        # Run inference.
        context.execute_async_v2(bindings=bindings, stream_handle=stream.handle)

        # Transfer predictions back from the GPU.
        [cuda.memcpy_dtoh_async(out.host, out.device, stream) for out in outputs]

        # Synchronize the stream
        stream.synchronize()
        # Return only the host outputs and inference time

        return [out.host for out in outputs]
    
    
