import tensorrt as trt
import numpy as np

def get_weights(weights, layer, param, sub_layer):
    if sub_layer == None:
        return weights[layer + "." + param].numpy()
    else:
        return weights[layer + "." + sub_layer + "." + param].numpy()


def conv3x3(network, weights, input, output_chs, 
            layer, sub_layer, stride=(1,1), padding=(0,0)):
    """3x3 conv using TensorRT IConvolutionLayer"""
    # Initialize weights
    conv_weights = get_weights(weights, layer, "weight", sub_layer)

    # Add 3x3 convolution layer
    Conv3x3 = network.add_convolution(input, 
                                    output_chs, 
                                    kernel_shape=(3,3), 
                                    kernel = conv_weights)

    # Configure stride and padding
    Conv3x3.stride = stride
    Conv3x3.padding = padding
    return Conv3x3

def conv1x1(network, weights, input, output_chs,
            layer, sub_layer, stride=(1,1), padding=(0,0)):
    """1x1 conv using TensorRT IConvolutionLayer"""
    # Initialize weights
    downsample_weights = get_weights(weights, layer, "weight", sub_layer)

    # Add 1x1 convolution layer
    Conv1x1 = network.add_convolution(input, 
                                    output_chs, 
                                    kernel_shape=(1,1), 
                                    kernel = downsample_weights)
    Conv1x1.stride = stride
    Conv1x1.padding = padding
    return Conv1x1

def upsample(network, input):
    resize_layer = network.add_resize(input)
    resize_layer.scales = [1,1,2,2]
    resize_layer.resize_mode = trt.ResizeMode.NEAREST
    return resize_layer

def elu(network, input):
    return network.add_activation(input, type=trt.ActivationType.ELU)    

def relu(network, input):
    """ReLU Layer using TensorRT IActivationLayer"""
    return network.add_activation(input, type=trt.ActivationType.RELU)

def maxpool(network, input, window_size=(3,3)):
    """Max Pooling Layer using TensorRT IPoolingLayer"""
    Maxpool = network.add_pooling(input, type=trt.PoolingType.MAX, window_size=window_size)
    Maxpool.stride = (2,2)
    Maxpool.padding = (1,1)
    return Maxpool

def bn_scaleshift(weight, bias, mean, var2):
    var = np.sqrt(var2 + 1e-5)
    scale = np.divide(weight, var)
    shift = np.subtract(bias, np.multiply(weight, np.divide(mean, var)))
    return scale, shift

def batchnorm(network, input, weights, layer, sub_layer):
    """Batch Normalization layer using TensorRT IScalingLayer"""
    # Initialize parameters
    bn_weight = get_weights(weights, layer, "weight", sub_layer)
    bn_bias = get_weights(weights, layer, "bias", sub_layer)
    bn_mean = get_weights(weights, layer, "running_mean", sub_layer)
    bn_var = get_weights(weights, layer, "running_var", sub_layer)

    # Calculate linear transformation parameters
    bn_scale, bn_shift = bn_scaleshift(bn_weight, bn_bias, bn_mean, bn_var)

    # Add 2D batch normalization layer
    BatchNorm = network.add_scale(input, mode=trt.ScaleMode.CHANNEL, scale=bn_scale, shift=bn_shift)
    return BatchNorm